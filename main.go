package main

import (
	"log"
	"myexercise/app/config"
	"myexercise/app/lib"
	"myexercise/app/routes"

	"github.com/gofiber/fiber/v2"
	"github.com/spf13/viper"
)

func init() {
	lib.LoadEnvironment(config.Environment)
}

// @title My Project
// @version 1.0.0
// @description API Documentation
// @termsOfService https://dospecs.monstercode.net/en/guide/tnc.html
// @contact.name Developer
// @contact.email developer.team.tog@gmail.com
// @host localhost:9000
// @schemes http
// @BasePath /api/v1
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func main() {
	// Clean Code 97.6%
	// content-type berhubungan dengan payload
	// contoh accept ada di bagian negative case
	// of "getdata.go" dan "postdata.go"
	// yang pake payload put, post, patch
	// yang ga pake payload tidak boleh pake content-type
	app := fiber.New(fiber.Config{
		Prefork: viper.GetString("PREFORK") == "true",
	})

	routes.Handle(app)
	log.Fatal(app.Listen(":" + viper.GetString("PORT")))
}
