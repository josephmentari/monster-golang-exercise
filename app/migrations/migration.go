package migrations

import "myexercise/app/model"

// ModelMigrations models to automigrate
var ModelMigrations = []interface{}{
	&model.Identity{},
	&model.Company{},
	&model.CreditCard{},
	&model.Handphone{},
}
