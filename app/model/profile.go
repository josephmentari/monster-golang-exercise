package model

import (
	"github.com/go-openapi/strfmt"
	"github.com/google/uuid"
)

type Identity struct {
	Base                     // Person ID; date & time created, updated, and deleted
	Birthdate   *strfmt.Date `json:"birthdate" gorm:"type:timestamptz" format:"date" swaggertype:"date"`
	Name        string       `json:"nickname" example:"Joseph Angelus"`                						// Person Name
	Age         int          `json:"age" example:"22"`                                 						// Person Age
	Education   string       `json:"education" example:"Bachelor of Computer Science"` 						// Person Last Education
	Height      float64      `json:"height" example:"175.5"`
	SingleOrNot bool         `json:"single" example:"true"`
	Description JSONMap      `json:"desc"`
	Company     Company      `json:"company,omitempty" gorm:"references:ID"`								// Belongs to Association
	CardID      *uuid.UUID   `json:"card_id" example:"1"`													// Has One Association
	CreditCard  CreditCard   `json:"credit_card,omitempty" gorm:"foreignKey:CardID;references:ID"`
	//HandphoneID *uuid.UUID	 `json:"phone_id" example:"1"`												
	Handphone   []Handphone  `json:"handphone,omitempty" gorm:"references:ID"`								// Has Many Association
}

type Company struct {
	Base
	Name       string     `json:"company_name" example:"Tokopedia"`
	Division   string     `json:"division" example:"IT"`
	IdentityID *uuid.UUID `json:"identity_id" example:"1"`
}

type CreditCard struct {
	Base
	Occupation string `json:"occupation" example:"programmer"`
	Property   string `json:"property" example:"house"`
}

type Handphone struct {
	Base
	PhoneName   string     `json:"phone_name" example:"Apple"`
	PhoneSeries string     `json:"phone_series" example:"14 Pro Max"`
	IdentityID  *uuid.UUID `json:"identity_id_phone" example:"1"`
}

// kalo belong to
// di identity struct ada creditcard , gak ada creditID
// di credit card struct ada identityID

// kalo has one
// di identiy struct ada cardID sama creditCard
// di credit card struct tidak ada identityID

// Table Relation - Belong to
// type Identity struct {
// 	Base                            // Person ID; date & time created, updated, and deleted
// 	Birthdate          *strfmt.Date `json:"birthdate" gorm:"type:timestamptz" format:"date" swaggertype:"date"`
// 	Name               string       `json:"nickname" example:"Joseph Angelus"` // Person Name
// 	Age                int          `json:"age" example:"22"`                  // Person Age
// 	CurrentEducationID int
// 	CurrentEducation   CurrentEducation `gorm:"foreignKey:CurrentEducationID, references:CurrentEducationID"`
// 	Height             float64          `json:"height" example:"175.5"`
// 	SingleOrNot        bool             `json:"single" example:"true"`
// 	Description        JSONMap          `json:"desc"`
// 	//ID        int    `json:"id"`
// }

// type CurrentEducation struct {
// 	CurrentEducationID int    `json:"education_id"`
// 	School_Name        string `json:"school_name" example:"Gloria Christian School"`
// 	CurrentEducation   string `json:"education" example:"Bachelor of Computer Science"` // Person Last Education
// }
