package routes

import (
	"myexercise/app/controller"
	"myexercise/app/services"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/spf13/viper"
)

// Handle all request to route to controller
func Handle(app *fiber.App) {
	app.Use(cors.New())
	services.InitDatabase()
	// services.InitRedis()

	api := app.Group(viper.GetString("ENDPOINT"))

	api.Post("/persons", controller.PostIdentity)

	api.Post("/persons/company", controller.PostCompany)

	api.Post("/persons/credit_card", controller.PostCreditCard)

	api.Post("/persons/phone", controller.PostPhone)

	api.Get("/persons", controller.GetIdentity)

	//api.Get("/persons/all", controller.GetAll(db))

	api.Get("/persons/company", controller.GetCompany)

	api.Get("/persons/credit_card", controller.GetCreditCard)

	api.Get("/persons/phone", controller.GetPhone)

	api.Get("/persons/:id", controller.GetIdentityWithID)

	api.Put("/persons/:id", controller.PutIdentity)

	api.Delete("/persons/:id", controller.DeleteIdentity)

	api.Delete("/persons/company/:id", controller.DeleteCompany)

	api.Delete("/persons/credit_card/:id", controller.DeleteCreditCard)

	api.Delete("/persons/phone/:id", controller.DeletePhone)

	api.Get("/", controller.GetAPIIndex)
	api.Get("/info.json", controller.GetAPIInfo)
}
