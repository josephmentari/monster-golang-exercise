package controller

import (
	"net/http"
	"testing"

	"github.com/gofiber/fiber/v2"
)

func BenchmarkGetIdentity(b *testing.B) {
	app := fiber.New()
	app.Get("/persons", GetIdentity)

	b.Run("GET /persons", func(b *testing.B) {
		req, _ := http.NewRequest("GET", "/persons", nil)
		req.Header.Set("accept", "application/json")

		for i := 0; i < b.N; i++ {
			app.Test(req)
		}
	})
}
