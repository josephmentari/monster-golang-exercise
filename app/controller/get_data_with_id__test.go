package controller

import (
	"net/http"
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
)

func TestGetDatawithID(t *testing.T) {
	// Positive Case of Unit Testing
	app := fiber.New()
	app.Get("/persons/:id", GetIdentityWithID)

	req, _ := http.NewRequest("GET", "/persons/:id", nil)
	req.Header.Set("accept", "application/json") // Accept Header
	res, err := app.Test(req)

	utils.AssertEqual(t, nil, err, "send request")
	utils.AssertEqual(t, 200, res.StatusCode, "get response")

	// Negative Case of Unit Testing
	req, _ = http.NewRequest("GET", "/persons/:id", nil)
	res, err = app.Test(req)

	utils.AssertEqual(t, nil, err, "send request")
	utils.AssertEqual(t, 400, res.StatusCode, "get response")
}
