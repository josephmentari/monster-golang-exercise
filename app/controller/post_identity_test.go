package controller

import (
	"bytes"
	"net/http"
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"

	"myexercise/app/model"
	"myexercise/app/services"
)

func TestPostPerson(t *testing.T) {
	// Positive Case of Unit Testing
	app := fiber.New()
	app.Post("/persons", PostIdentity)

	db := services.InitDatabaseForTest()
	profile := model.Identity{}
	profile.Name = "Mike"
	profile.Age = 25
	profile.Education = "Collage Student"
	profile.Height = 170
	profile.SingleOrNot = false
	profile.Description = model.JSONMap(map[string]interface{}{"hobby": "play table tennis", "occupation": "student"})
	db.Create(&profile)
	profileID := profile.ID

	card := model.CreditCard{}
	card.Occupation = "Game Developer"
	card.Property = "Apartment"

	db.Create(&card)
	cardId := card.ID

	payload := bytes.NewReader([]byte(`
		{
			"birthdate":"2000-12-21",
			"name":"John Doe",
			"age":24,
			"education":"High School Student",
			"height":170,
			"single?":true,
			"desc":{"hobby":"play table tennis", "occupation":"student"},
			"company":{"company_name":"Tokopedia","division":"management","identity_id":"` + profileID.String() + `"},
			"card_id": "` + cardId.String() + `",
			"handphone":[{"phone_name":"iPhone", "phone_series":"14 pro max"}, {"phone_name":"Samsung", "phone_series":"S22"}]
		}
	`))

	req, _ := http.NewRequest("POST", "/persons", payload)
	req.Header.Set("accept", "application/json")
	req.Header.Set("content-type", "application/json")
	res, err := app.Test(req)

	utils.AssertEqual(t, nil, err, "sending request")
	utils.AssertEqual(t, 200, res.StatusCode, "response code")

	// Negative Case of Unit Testing
	req, _ = http.NewRequest("POST", "/persons", nil)
	res, err = app.Test(req)

	utils.AssertEqual(t, nil, err, "send request")
	utils.AssertEqual(t, 400, res.StatusCode, "get response")

	req, _ = http.NewRequest("POST", "/persons", nil)
	req.Header.Set("accept", "application/json")
	req.Header.Set("content-type", "application/json")
	res, err = app.Test(req)

	utils.AssertEqual(t, nil, err, "send request")
	utils.AssertEqual(t, 400, res.StatusCode, "get response")

	// Unit testing for status code 404
	// newProfile := model.Identity{}
	// db.Create(&profile)
	// newProfileID := newProfile.ID
	// newPayload := bytes.NewReader([]byte(`
	// 	{
	// 		"card_id": "` + newProfileID.String() + `"
	// 	}
	// `))

	// req, _ = http.NewRequest("POST", "/persons", newPayload)
	// req.Header.Set("accept", "application/json")
	// req.Header.Set("content-type", "application/json")
	// res, err = app.Test(req)

	// utils.AssertEqual(t, nil, err, "sending request")
	// utils.AssertEqual(t, 404, res.StatusCode, "response code")

}
