package controller

import (
	"bytes"
	"net/http"
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
)

func TestPostPerson3(t *testing.T) {
	// Positive Case of Unit Testing
	app := fiber.New()
	app.Post("/persons/credit_card", PostCreditCard)

	payload := bytes.NewReader([]byte(`
		{
			"card_owner":"Joseph",
			"occupation":"IT"
		}
	`))

	req, _ := http.NewRequest("POST", "/persons/credit_card", payload)
	req.Header.Set("accept", "application/json")
	req.Header.Set("content-type", "application/json")
	res, err := app.Test(req)

	utils.AssertEqual(t, nil, err, "sending request")
	utils.AssertEqual(t, 200, res.StatusCode, "response code")

	// Negative Case of Unit Testing
	req, _ = http.NewRequest("POST", "/persons/credit_card", nil)
	res, err = app.Test(req)

	utils.AssertEqual(t, nil, err, "send request")
	utils.AssertEqual(t, 400, res.StatusCode, "get response")

	req, _ = http.NewRequest("POST", "/persons/credit_card", nil)
	req.Header.Set("accept", "application/json")
	req.Header.Set("content-type", "application/json")
	res, err = app.Test(req)

	utils.AssertEqual(t, nil, err, "send request")
	utils.AssertEqual(t, 400, res.StatusCode, "get response")

}
