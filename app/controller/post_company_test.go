package controller

import (
	"bytes"
	"net/http"
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
)

func TestPostPerson2(t *testing.T) {
	// Positive Case of Unit Testing
	app := fiber.New()
	app.Post("/persons/company", PostCompany)

	payload := bytes.NewReader([]byte(`
		{
			"company_name":"Monster Group",
			"division":"IT",
			"identity_id":"2adf5565-484a-4d47-b2a6-0530ba3432a6"
		}
	`))

	req, _ := http.NewRequest("POST", "/persons/company", payload)
	req.Header.Set("accept", "application/json")
	req.Header.Set("content-type", "application/json")
	res, err := app.Test(req)

	utils.AssertEqual(t, nil, err, "sending request")
	utils.AssertEqual(t, 200, res.StatusCode, "response code")

	// Negative Case of Unit Testing
	req, _ = http.NewRequest("POST", "/persons/company", nil)
	res, err = app.Test(req)

	utils.AssertEqual(t, nil, err, "send request")
	utils.AssertEqual(t, 400, res.StatusCode, "get response")

	req, _ = http.NewRequest("POST", "/persons/company", nil)
	req.Header.Set("accept", "application/json")
	req.Header.Set("content-type", "application/json")
	res, err = app.Test(req)

	utils.AssertEqual(t, nil, err, "send request")
	utils.AssertEqual(t, 400, res.StatusCode, "get response")

}
