package controller

import (
	"strings"

	"github.com/gofiber/fiber/v2"

	"myexercise/app/model"
	"myexercise/app/services"
)

// postData insert all companies
// @Summary insert companies into array
// @Description insert companies into array
// @Accept application/json
// @Produce application/json
// @Success 200 {object} []model.Company "success"
// @Router /persons/company [post]
// @Tags company
func PostCompany(c *fiber.Ctx) error {
	// Negative Case of Unit Testing
	accept := c.Get("accept")
	if !strings.EqualFold(accept, "application/json") {
		return c.Status(400).JSON(fiber.Map{
			"message": "invalid accept header",
		})
	}

	var company model.Company
	if err := c.BodyParser(&company); err != nil {
		return c.Status(400).JSON((fiber.Map{
			"message": "invalid request",
		}))
	}

	// Positive Case of Unit Testing
	db := services.DB
	db.AutoMigrate(&model.Company{})

	db.Create(&company)

	return c.JSON(company)
}
