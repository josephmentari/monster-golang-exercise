package controller

import (
	"myexercise/app/model"
	"myexercise/app/services"
	"net/http"
	"testing"

	//"time"

	//"github.com/go-openapi/strfmt"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
)

func TestDeletePerson(t *testing.T) {
	services.InitDatabaseForTest()
	db := services.DB

	app := fiber.New()
	app.Delete("/persons/:id", DeleteIdentity)

	profile := model.Identity{}
	//education := model.CurrentEducation{}
	//*profile.Birthdate = strfmt.Date(time.Date(2000, 12, 21, 0, 0, 0, 0, time.UTC))
	profile.Name = "Mike"
	profile.Age = 25

	profile.Education = "Collage Student"
	// education.CurrentEducationID = 001
	// education.School_Name = "Gloria Christian School"
	// education.CurrentEducation = "Collage Student"

	profile.Height = 170
	profile.SingleOrNot = false
	profile.Description = model.JSONMap(map[string]interface{}{"hobby": "play table tennis", "occupation": "student"})
	// profile.Company = "{'company_name':'Monster Group', 'division':'IT'}"
	// *profile.CardId = "2adf5565-484a-4d47-b2a6-0530ba3432a6"
	// profile.Handphone = "[{'phone_name':'iPhone', 'phone_series':'14 pro max'}, {'phone_name':'Samsung', 'phone_series':'S22'}]"

	db.Create(&profile)
	id := profile.ID

	// Positive Case of Unit Testing
	req, _ := http.NewRequest("DELETE", "/persons/"+id.String(), nil)
	req.Header.Set("accept", "application/json")
	res, err := app.Test(req)

	utils.AssertEqual(t, nil, err, "sending request")
	utils.AssertEqual(t, 200, res.StatusCode, "response code")

	// Negative Case of Unit Testing
	req, _ = http.NewRequest("DELETE", "/persons/"+id.String(), nil)
	res, err = app.Test(req)

	utils.AssertEqual(t, nil, err, "send request")
	utils.AssertEqual(t, 400, res.StatusCode, "get response")

	req, _ = http.NewRequest("DELETE", "/persons/1", nil)
	req.Header.Set("accept", "application/json")
	res, err = app.Test(req)

	utils.AssertEqual(t, nil, err, "sending request")
	utils.AssertEqual(t, 404, res.StatusCode, "response code")

	//db.Where(`id = ?`, id).Delete(&model.Identity{})
}
