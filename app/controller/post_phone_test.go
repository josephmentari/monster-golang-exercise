package controller

import (
	"bytes"
	"net/http"
	"testing"

	"myexercise/app/model"
	"myexercise/app/services"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
)

func TestPostPerson4(t *testing.T) {
	app := fiber.New()
	app.Post("/persons/phone", PostPhone)

	db := services.InitDatabaseForTest()
	profile := model.Identity{}
	profile.Name = "Mike"
	profile.Age = 25
	profile.Education = "Collage Student"
	profile.Height = 170
	profile.SingleOrNot = false
	profile.Description = model.JSONMap(map[string]interface{}{"hobby": "play table tennis", "occupation": "student"})
	db.Create(&profile)
	profileID := profile.ID

	// Positive Case of Unit Testing
	payload := bytes.NewReader([]byte(`
		{
			"phone_name":"iPhone",
			"phone_series":"14 Pro Max",
			"identity_id_phone": "` + profileID.String() + `"
		}
	`))

	// payload = bytes.NewReader([]byte(`
	// {
	// 	"reason": "ini alasan",
	// 	"date_start": "` + start.String() + `",
	// 	"leave_allocation_id": "` + leave.ID.String() + `"
	// }
	// `))

	req, _ := http.NewRequest("POST", "/persons/phone", payload)
	req.Header.Set("accept", "application/json")
	req.Header.Set("Content-Type", "application/json")
	res, err := app.Test(req)

	utils.AssertEqual(t, nil, err, "sending request")
	utils.AssertEqual(t, 200, res.StatusCode, "response code")

	// Negative Case of Unit Testing
	req, _ = http.NewRequest("POST", "/persons/phone", nil)
	res, err = app.Test(req)

	utils.AssertEqual(t, nil, err, "send request")
	utils.AssertEqual(t, 400, res.StatusCode, "get response")

	req, _ = http.NewRequest("POST", "/persons/phone", nil)
	req.Header.Set("accept", "application/json")
	req.Header.Set("Content-Type", "application/json")
	res, err = app.Test(req)

	utils.AssertEqual(t, nil, err, "send request")
	utils.AssertEqual(t, 400, res.StatusCode, "get response")

}
