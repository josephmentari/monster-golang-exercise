package controller

import (
	"strings"

	"github.com/gofiber/fiber/v2"

	"myexercise/app/model"
	"myexercise/app/services"
)

// postData insert credit card
// @Summary insert credit card into array
// @Description insert credit card into array
// @Accept application/json
// @Produce application/json
// @Success 200 {object} []model.CreditCard "success"
// @Router /persons/credit_card [post]
// @Tags credit_card
func PostCreditCard(c *fiber.Ctx) error {
	// Negative Case of Unit Testing
	accept := c.Get("accept")
	if !strings.EqualFold(accept, "application/json") {
		return c.Status(400).JSON(fiber.Map{
			"message": "invalid accept header",
		})
	}

	db := services.DB
	var creditCard model.CreditCard
	if err := c.BodyParser(&creditCard); err != nil {
		return c.Status(400).JSON((fiber.Map{
			"message": "invalid request",
		}))
	}
	// var card model.CreditCard
	// identity := model.Identity{}
	// profile := db.Where(`id = ?`, card.IdentityID).First(&identity)
	// if profile.RowsAffected < 1 {
	// 	return lib.ErrorNotFound(c, "Identity not found")
	// }

	// Positive Case of Unit Testing
	db.AutoMigrate(&model.CreditCard{})

	db.Create(&creditCard)

	return c.JSON(creditCard)
}
