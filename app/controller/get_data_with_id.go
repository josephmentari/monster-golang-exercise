package controller

import (
	"myexercise/app/model"
	"myexercise/app/services"
	"strings"

	"github.com/gofiber/fiber/v2"
)

// getDatawithID get persons with id
// @Summary show persons identity with certain id
// @Description show persons identitiy with certain
// @Accept application/json
// @Produce application/json
// @Success 200 {object} []model.Identity "success"
// @Router /persons [get]
// @Tags persons
func GetIdentityWithID(c *fiber.Ctx) error {
	// Negative Case of Unit Testing
	accept := c.Get("accept")
	if !strings.EqualFold(accept, "application/json") {
		return c.Status(400).JSON(fiber.Map{
			"message": "invalid accept header",
		})
	}

	// Positive Case of Unit Testing
	id := c.Params("id")
	//log.Println(id)
	db := services.DB
	var persons model.Identity
	//db.Model(&model.Identity{}).Find(&(persons[newIndex]))
	db.Where(`id = ?`, id).First(&persons)
	//log.Println("person", persons)

	//message := fmt.Sprintf("id %s found", id)

	return c.JSON(persons)
}
