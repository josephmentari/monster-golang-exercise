package controller

import (
	"myexercise/app/model"
	"myexercise/app/services"
	"net/http"
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
)

func TestDeletePerson4(t *testing.T) {
	services.InitDatabaseForTest()
	db := services.DB
	// Positive Case of Unit Testing
	app := fiber.New()
	app.Delete("/persons/phone/:id", DeletePhone)

	phone := model.Handphone{}
	phone.PhoneName = "iPhone"
	phone.PhoneSeries = "14 Pro Max"

	db.Create(&phone)
	id := phone.ID

	req, _ := http.NewRequest("DELETE", "/persons/phone/"+id.String(), nil)
	req.Header.Set("accept", "application/json")
	res, err := app.Test(req)

	utils.AssertEqual(t, nil, err, "sending request")
	utils.AssertEqual(t, 200, res.StatusCode, "response code")

	// Negative Case of Unit Testing
	req, _ = http.NewRequest("DELETE", "/persons/phone/"+id.String(), nil)
	res, err = app.Test(req)

	utils.AssertEqual(t, nil, err, "send request")
	utils.AssertEqual(t, 400, res.StatusCode, "get response")

	req, _ = http.NewRequest("DELETE", "/persons/phone/1", nil)
	req.Header.Set("accept", "application/json")
	res, err = app.Test(req)

	utils.AssertEqual(t, nil, err, "sending request")
	utils.AssertEqual(t, 404, res.StatusCode, "response code")

	//db.Where(`id = ?`, id).Delete(&model.Identity{})
}
