package controller

import (
	"fmt"
	//"log"
	"myexercise/app/model"
	"myexercise/app/services"

	"github.com/gofiber/fiber/v2"
)

// putData change persons value
// @Summary change persons identity with id
// @Description change persons identitiy with id
// @Accept application/json
// @Produce application/json
// @Success 200 {object} []model.Identity "success"
// @Router /persons [put]
// @Tags persons
func PutIdentity(c *fiber.Ctx) error {
	// Negative Case of Unit Testing
	// accept := c.Get("accept")
	// if !strings.EqualFold(accept, "application/json") {
	// 	return c.Status(400).JSON(fiber.Map{
	// 		"message": "invalid accept header",
	// 	})
	// }

	var person model.Identity
	if err := c.BodyParser(&person); err != nil {
		return c.Status(400).JSON((fiber.Map{
			"message": "invalid request",
		}))
	}

	// log.Println(&person)
	var newPerson = person
	id := c.Params("id")
	db := services.DB
	/* value dari model atau variable newPerson akan berubah setelah query dijalankan
	karena hasil query tersebut akan ditampung di variabel newPerson
	jadi jika tidak buat temp var model, valuenya akan ditumpuk */
	testID := db.Where(`id = ?`, id).First(&newPerson)
	//log.Println(testID)
	/* RowsAffected berfungsi untuk mengecek ada berapa row dari hasil query */
	if testID.RowsAffected < 1 {
		return c.Status(404).JSON(fiber.Map{
			"message": "not found",
		})
	}

	// log.Println(c.Params("id"))
	// log.Println(c.Params("birthdate"))
	// log.Println(c.Params("nickname"))
	// log.Println(c.Params("age"))
	// log.Println(c.Params("education"))
	// log.Println(c.Params("height"))
	// log.Println(c.Params("single"))
	// log.Println()
	// log.Println(person.Birthdate)
	// log.Println(person.Name)
	// log.Println(person.Age)
	// log.Println(person.Education)
	// log.Println(person.Height)
	// log.Println(person.SingleOrNot)
	// log.Println()
	// log.Println(c.BodyParser(&person))
	// log.Println()
	// log.Println(&person)

	// Positive Case of Unit Testing
	db.Where(`id = ?`, id).Updates(&person)
	/* db.Model(&person).Updates(map[string]interface{}{
	"birthdate": c.Params("birthdate"),
	"name":      c.Params("nickname"),
	"age":       c.Params("age"),
	"education": c.Params("education"),
	"height":    c.Params("height"),
	"single":    c.Params("single"),
	"decs":		 c.Params("decs"),
	"company":	 c.Params("company"),
	"cardid":	 c.Params("cardid"),
	"handphone": c.Params("handphone")}), */

	message := fmt.Sprintf(`person with id %s has been updated`, id)

	return c.JSON(fiber.Map{
		"message": message,
	})
}
