package controller

import (
	"strings"

	"github.com/gofiber/fiber/v2"

	//"myexercise/app/lib"
	"myexercise/app/model"
	"myexercise/app/services"
)

// postData insert all persons
// @Summary insert person identity into array
// @Description insert person identitiy into array
// @Accept application/json
// @Produce application/json
// @Success 200 {object} []model.Identity "success"
// @Router /persons [post]
// @Tags persons
func PostIdentity(c *fiber.Ctx) error {
	// Negative Case of Unit Testing
	accept := c.Get("accept")
	if !strings.EqualFold(accept, "application/json") {
		return c.Status(400).JSON(fiber.Map{
			"message": "invalid accept header",
		})
	}
	// dari sini sampe status code 409 blm ada testing-nya!!
	db := services.DB
	var person model.Identity
	if err := c.BodyParser(&person); err != nil {
		return c.Status(400).JSON((fiber.Map{
			"message": "invalid request",
		}))
	}

	/* NOTES
	//company := model.Company{}
	// var persons []model.Identity
	// com := db.Where(`company_name = ? AND division = ?`, person.Company.Name, person.Company.Division).First(&persons)
	// if com.RowsAffected < 1 {
	// 	return lib.ErrorNotFound(c, "Company not found")
	// }
	// testID := db.Where(`id = ?`, person.ID).First(&persons)
	// if testID.RowsAffected < 1 {
	// 	return c.Status(409).JSON(fiber.Map{
	// 		"message": "data already exist",
	// 	})
	// }
	*/

	// status code 404 -> cari tau cara testing-nya!!
	// creditCard := model.CreditCard{}
	// card := db.Where(`id = ?`, person.CardID).First(&creditCard)
	// if card.RowsAffected < 1 {
	// 	return lib.ErrorNotFound(c, "Credit Card not found")
	// }

	// Positive Case of Unit Testing
	db.AutoMigrate(&model.Identity{})

	db.Create(&person)

	return c.JSON(person)
}
