package controller

import (
	"myexercise/app/model"
	"myexercise/app/services"
	"strings"

	"github.com/gofiber/fiber/v2"
)

// getData get all companies
// @Summary show all companies identity in array
// @Description show all companies identitiy in array
// @Accept application/json
// @Produce application/json
// @Success 200 {object} []model.Company "success"
// @Router /persons/2 [get]
// @Tags companies
func GetCreditCard(c *fiber.Ctx) error {
	// Negative Case of Unit Testing
	accept := c.Get("accept")
	if !strings.EqualFold(accept, "application/json") {
		return c.Status(400).JSON(fiber.Map{
			"message": "invalid accept header",
		})
	}

	// Positive Case of Unit Testing
	db := services.DB
	var credit_card []model.CreditCard
	db.Model(&model.CreditCard{}).Find(&credit_card)

	return c.JSON(credit_card)
}

// Retrieve user list with eager loading credit card
// func GetAll(db *gorm.DB) ([]model.Identity, error) {
// 	var identities []model.Identity
// 	err := db.Model(&model.Identity{}).Preload("CreditCard").Find(&identities).Error
// 	return identities, err
// }
