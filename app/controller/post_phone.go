package controller

import (
	"strings"

	"github.com/gofiber/fiber/v2"

	"myexercise/app/model"
	"myexercise/app/services"
)

// postData insert all phones
// @Summary insert phones into array
// @Description insert phones into array
// @Accept application/json
// @Produce application/json
// @Success 200 {object} []model.Handphone "success"
// @Router /persons/phone [post]
// @Tags phone
func PostPhone(c *fiber.Ctx) error {
	// Negative Case of Unit Testing
	accept := c.Get("accept")
	if !strings.EqualFold(accept, "application/json") {
		return c.Status(400).JSON(fiber.Map{
			"message": "invalid accept header",
		})
	}

	var phone model.Handphone
	if err := c.BodyParser(&phone); err != nil {
		return c.Status(400).JSON((fiber.Map{
			"message": "invalid request",
		}))
	}

	// Positive Case of Unit Testing
	db := services.DB
	db.AutoMigrate(&model.Handphone{})

	db.Create(&phone)

	return c.JSON(phone)
}
