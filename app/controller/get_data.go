package controller

import (
	"myexercise/app/model"
	"myexercise/app/services"
	"strings"

	"github.com/gofiber/fiber/v2"
)

// getData get all persons
// @Summary show all persons identity in array
// @Description show all persons identitiy in array
// @Accept application/json
// @Produce application/json
// @Success 200 {object} []model.Identity "success"
// @Router /persons [get]
// @Tags persons
func GetIdentity(c *fiber.Ctx) error {
	db := services.DB
	var persons []model.Identity

	// Negative Case of Unit Testing
	accept := c.Get("accept")
	if !strings.EqualFold(accept, "application/json") {
		return c.Status(400).JSON(fiber.Map{
			"message": "invalid accept header",
		})
	}

	// Positive Case of Unit Testing
	// var company []model.Company
	//db.Model(&model.Company{}).Association("Company")
	db.Model(&model.Identity{}).Joins(`Company`).Joins(`CreditCard`).Preload(`Handphone`).Find(&persons)

	return c.JSON(persons)
}
