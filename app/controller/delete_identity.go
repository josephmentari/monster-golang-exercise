package controller

import (
	"fmt"
	"myexercise/app/model"
	"myexercise/app/services"
	"strings"

	"github.com/gofiber/fiber/v2"
)

// deleteData delete certain data
// @Summary delete persons identity with id
// @Description delete persons identitiy with id
// @Accept application/json
// @Produce application/json
// @Success 200 {object} []model.Identity "success"
// @Router /persons [delete]
// @Tags persons
func DeleteIdentity(c *fiber.Ctx) error {
	// if err := c.BodyParser(&person); err != nil {
	// 	return c.Status(400).JSON((fiber.Map{
	// 		"message": "invalid request",
	// 	}))
	// }

	// Negative Case of Unit Testing
	accept := c.Get("accept")
	if !strings.EqualFold(accept, "application/json") {
		return c.Status(400).JSON(fiber.Map{
			"message": "invalid accept header",
		})
	}

	var person model.Identity
	id := c.Params("id")
	db := services.DB
	testID := db.Where(`id = ?`, id).First(&person)
	if testID.RowsAffected < 1 {
		return c.Status(404).JSON(fiber.Map{
			"message": "not found",
		})
	}

	// Positive Case of Unit Testing
	db.Where(`id = ?`, id).Delete(&model.Identity{})

	message := fmt.Sprintf("person with id %s has been deleted", id)

	return c.JSON((fiber.Map{
		"message": message,
	}))
}
