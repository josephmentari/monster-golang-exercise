package controller

import (
	"myexercise/app/model"
	"myexercise/app/services"
	"strings"

	"github.com/gofiber/fiber/v2"
)

// getData get all phones
// @Summary show all phones in array
// @Description show all phones in array
// @Accept application/json
// @Produce application/json
// @Success 200 {object} []model.Handphone "success"
// @Router /persons/phone [get]
// @Tags phone
func GetPhone(c *fiber.Ctx) error {
	// Negative Case of Unit Testing
	accept := c.Get("accept")
	if !strings.EqualFold(accept, "application/json") {
		return c.Status(400).JSON(fiber.Map{
			"message": "invalid accept header",
		})
	}

	// Positive Case of Unit Testing
	db := services.DB
	var phone []model.Handphone
	db.Model(&model.Handphone{}).Find(&phone)

	return c.JSON(phone)
}
