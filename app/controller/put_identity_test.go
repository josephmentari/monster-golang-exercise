package controller

import (
	"bytes"
	"myexercise/app/model"
	"myexercise/app/services"
	"net/http"
	"testing"

	//"time"

	//"github.com/go-openapi/strfmt"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
)

func TestPutPerson(t *testing.T) {
	db := services.InitDatabaseForTest()
	//db := services.DB

	// Positive Case of Unit Testing
	app := fiber.New()
	app.Put("/persons/:id", PutIdentity)

	profile := model.Identity{}
	//*profile.Birthdate = strfmt.Date(time.Date(2000, 12, 21, 0, 0, 0, 0, time.UTC))
	profile.Name = "Mike"
	profile.Age = 25
	profile.Education = "Collage Student"
	profile.Height = 170
	profile.SingleOrNot = false
	profile.Description = model.JSONMap(map[string]interface{}{"hobby": "play table tennis", "occupation": "student"})

	db.Create(&profile)
	id := profile.ID

	// card := model.CreditCard{}
	// card.Occupation = "Game Developer"
	// card.Property = "Apartment"

	// db.Create(&card)
	// cardId := card.ID

	payload := bytes.NewReader([]byte(`
		{
			"birthdate":"2000-12-21",
			"nickname":"John Doe",
			"age":24,
			"education":"High School Student",
			"height":170,
			"single":true
		}
	`))

	// key dan value dari struct identity dibawah ini mending dihapus krn ganggu di payload
	// "desc":"{"hobby":"play table tennis", "occupation":"student"}",
	// "company":"{"company_name":"Tokopedia","division":"management","identity_id":"` + id.String() + `"}",
	// "card_id": "` + cardId.String() + `",
	// "handphone":["{"phone_name":"iPhone", "phone_series":"14 pro max", "identity_id_phone": "` + id.String() + `"}, {"phone_name":"Samsung", "phone_series":"S22", "identity_id_phone": "` + id.String() + `"}"]

	req, _ := http.NewRequest("PUT", "/persons/"+id.String(), payload)
	req.Header.Set("accept", "application/json")
	req.Header.Set("Content-Type", "application/json")
	res, err := app.Test(req)

	utils.AssertEqual(t, nil, err, "sending request")
	utils.AssertEqual(t, 200, res.StatusCode, "response code")

	// Negative Case of Unit Testing
	req, _ = http.NewRequest("PUT", "/persons/"+id.String(), nil)
	res, err = app.Test(req)

	utils.AssertEqual(t, nil, err, "send request")
	utils.AssertEqual(t, 400, res.StatusCode, "get response")

	/* profile2 := model.Identity{}
	//*profile.Birthdate = strfmt.Date(time.Date(2000, 12, 21, 0, 0, 0, 0, time.UTC))
	profile2.Name = "Mike"
	profile2.Age = 25
	profile2.Education = "Collage Student"
	profile2.Height = 170
	profile2.SingleOrNot = false
	profile2.Description = model.JSONMap(map[string]interface{}{"hobby": "play table tennis", "occupation": "student"})

	db.Create(&profile2)
	id2 := profile2.ID

	payload = bytes.NewReader([]byte(`
		{
			"nickname":"John Doe",
			"age":24,
			"education":"High School Student",
			"height":170,
			"single":true,
			"desc":"{"hobby":"play table tennis", "occupation":"student"}",
			"company":{"company_name":"Tokopedia","division":"management","identity_id":"` + id2.String() + `"},
			"card_id": "` + id2.String() + `",
			"handphone":[{"phone_name":"iPhone", "phone_series":"14 pro max"}, {"phone_name":"Samsung", "phone_series":"S22"}]
		}
	`)) */

	payload = bytes.NewReader([]byte(`
		{
			"birthdate":"2000-12-21",
			"nickname":"John Doe",
			"age":24,
			"education":"High School Student",
			"height":170,
			"single":true
		}
	`))

	req, _ = http.NewRequest("PUT", "/persons/1", payload)
	req.Header.Set("accept", "application/json")
	req.Header.Set("Content-Type", "application/json")
	res, err = app.Test(req)

	utils.AssertEqual(t, nil, err, "sending request")
	utils.AssertEqual(t, 404, res.StatusCode, "response code")

	//db.Where(`id = ?`, id).Delete(&model.Identity{})
}
