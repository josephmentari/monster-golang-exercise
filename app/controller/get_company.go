package controller

import (
	"myexercise/app/model"
	"myexercise/app/services"
	"strings"

	"github.com/gofiber/fiber/v2"
)

// getData get all companies
// @Summary show all companies in array
// @Description show all companies in array
// @Accept application/json
// @Produce application/json
// @Success 200 {object} []model.Company "success"
// @Router /persons/company [get]
// @Tags companies
func GetCompany(c *fiber.Ctx) error {
	// Negative Case of Unit Testing
	accept := c.Get("accept")
	if !strings.EqualFold(accept, "application/json") {
		return c.Status(400).JSON(fiber.Map{
			"message": "invalid accept header",
		})
	}

	// Positive Case of Unit Testing
	db := services.DB
	var company []model.Company
	db.Model(&model.Company{}).Find(&company)

	return c.JSON(company)
}
