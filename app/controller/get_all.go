package controller

import (
	"myexercise/app/model"

	"gorm.io/gorm"
)

// Retrieve user list with eager loading credit card
func GetAll(db *gorm.DB) ([]model.Identity, error) {
	var profile []model.Identity
	err := db.Model(&model.Identity{}).Preload("CreditCard").Find(&profile).Error
	return profile, err
}
